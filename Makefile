# ***********************************************
# ***            Loco363 - ModuCab            ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


## Directories (source, build/object, binaries/output)
# Ours
FS=src
FSM=$(FS)/main
FSL=$(FS)/lib
FST=$(FS)/tests

FO=build
FB=bin

# Base ModuCab ones
MC_F=modu-cab

MC_FS=$(MC_F)/src
MC_FSM=$(MC_FS)/main
MC_FST=$(MC_FS)/tests

MC_FO=$(MC_F)/build
MC_FB=$(MC_F)/bin


## Compiler settings
# Compiler binary
CC=g++

# Standard
CFS=-std=c++14

# Flags
CFO=$(CFS) -Wall -Werror -pedantic -c
CFD=$(CFS) -MM

# Start searching for includes in project's top-level source directories
CFFM=-iquote$(FSM) -iquote$(MC_FSM) -iquote$(FSL)
CFFT=-iquote$(FS) -iquote$(MC_FS) $(CFFM)

# Dependencies shortcut flags
CFDM=$(CFD) $(CFFM)
CFDT=$(CFD) $(CFFT)

# Modules shortcut flags
CFOM=$(CFO) $(CFFM)
CFOT=$(CFO) $(CFFT)


## General targets
# Default target
all: main tests

# Remove all generated files (also from base ModuCab)
clean:
	rm -f $(FO)/* $(FB)/*
	$(MAKE) -C $(MC_F) $@

# Main target
main: $(FB)/main

# Test target
tests: \
	$(FB)/tests---main-shmem \
	$(FB)/tests---parts_manual-indicator \
	$(FB)/tests---parts_manual-lever


## Dependencies
# Load all builded dependencies
include $(FO)/*.d

# when no .d file exists, make calls next rule which fails for '*.d' file
#   => due to that, the hack with test before invoking compiler itself is needed
# the hack after compiler removes dependency file and old object file in case of error during dependency generation
#   => otherwise, the bash creates empty dependency file which leads to broken dependencies
#   or even leaving the old object file untouched until invoking the clean target

# General rules for building
$(FO)/%.d:
	test '$*' = '*' \
		|| $(CC) $(CFDM) "$(FSM)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc" -MT '$$(FO)/$*.o $$(FO)/$*.d' > $@ \
		|| (rm $(FO)/$*.* && exit 1)

$(FO)/tests---%.d:
	$(CC) $(CFDT) "$(FST)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc" -MT '$$(FO)/tests---$*.o $$(FO)/tests---$*.d' > $@ \
		|| (rm $(FO)/tests---$*.* && exit 1)

# Do not delete the dependency files after first build (as this violates their purpose)
.PRECIOUS: \
	$(FO)/%.d \
	$(FO)/tests---%.d


## Modules - compilations
# General rules for compiling (all modules targets must always depend on corresponding .d dependency file, otherwise it is not builded, so not working at all)
$(FO)/%.o: $(FO)/%.d
	$(CC) -o $@ $(CFOM) "$(FSM)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc"

$(FO)/tests---%.o: $(FO)/tests---%.d
	$(CC) -o $@ $(CFOT) "$(FST)/`bash -c 'TMP=\"\$${0//_/\\/}\"; echo \"\$${TMP//-/_}\"' '$*'`.cc"

# Base ModuCab modules - pass compiling to its own Makefile (no dependencies here, as the git submodule is supposed to be stable)
$(MC_FO)/%.o:
	$(MAKE) -C $(MC_F) 'build/$*.o'

## Binaries - building
# Main
$(FB)/main: $(FO)/main.o \
		$(MC_FO)/core_sequential.o \
		$(FO)/devices_indicators_a-indicator.o \
		$(FO)/devices_indicators_on-off.o \
		$(FO)/devices_indicators_power-system.o \
		$(FO)/devices_mcc_a-mcc.o \
		$(FO)/devices_mcc_braking.o \
		$(FO)/devices_mcc_direction.o \
		$(FO)/devices_mcc_driving.o \
		$(FO)/devices_combi-meter.o \
		$(FO)/devices_switches_debouncer.o \
		$(FO)/devices_switches_rotary_fallback-control.o \
		$(FO)/devices_switches_rotary_pantographs.o \
		$(FO)/devices_switches_rotary_reflector.o \
		$(FO)/devices_switches_rotary_system.o \
		$(FO)/devices_switches_toggle_light.o \
		$(FO)/devices_switches_toggle_on-off.o \
		$(FO)/devices_switches_toggle_pos-light.o \
		$(MC_FO)/interfaces_i2c_i2c.o \
		$(MC_FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(FO)/interfaces_ipc_shared-memory.o \
		$(MC_FO)/interfaces_serial_a-serial.o \
		$(MC_FO)/interfaces_serial_posix.o \
		$(FO)/parts_indicator.o \
		$(FO)/parts_lever.o \
		$(MC_FO)/parts_pcf8574.o \
		$(MC_FO)/parts_pcf8591.o
	$(CC) -o $@ $^ -lrt -lpthread

# Tests
$(FB)/tests---main-shmem: $(FO)/tests---main-shmem.o \
		$(FO)/data-dumper.o
	$(CC) -o $@ $^ -lrt -lpthread

$(FB)/tests---parts_manual-indicator: $(FO)/tests---parts_manual-indicator.o \
		$(MC_FO)/tests---testing-tools_serial-debugger.o \
		$(MC_FO)/interfaces_i2c_i2c.o \
		$(MC_FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(MC_FO)/interfaces_serial_a-serial.o \
		$(MC_FO)/interfaces_serial_posix.o \
		$(FO)/parts_indicator.o
	$(CC) -o $@ $^

$(FB)/tests---parts_manual-lever: $(FO)/tests---parts_manual-lever.o \
		$(MC_FO)/tests---testing-tools_serial-debugger.o \
		$(MC_FO)/interfaces_i2c_i2c.o \
		$(MC_FO)/interfaces_i2c_drivers_bus-pirate-v4.o \
		$(MC_FO)/interfaces_serial_a-serial.o \
		$(MC_FO)/interfaces_serial_posix.o \
		$(FO)/parts_lever.o
	$(CC) -o $@ $^
