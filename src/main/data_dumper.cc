/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "data_dumper.h"

#include <cstdio> // FILE, fprintf
#include <stdexcept> // C++ standard exceptions

#include "mc-data/data/compressor_1_aux.h"
#include "mc-data/data/compressor_2.h"
#include "mc-data/data/control_mode.h"
#include "mc-data/data/coupling.h"
#include "mc-data/data/driving_mode.h"
#include "mc-data/data/fallback_control.h"
#include "mc-data/data/fans.h"
#include "mc-data/data/light.h"
#include "mc-data/data/mcc_braking.h"
#include "mc-data/data/mcc_direction.h"
#include "mc-data/data/mcc_driving.h"
#include "mc-data/data/on_off.h"
#include "mc-data/data/pantographs.h"
#include "mc-data/data/pos_light.h"
#include "mc-data/data/power_system.h"
#include "mc-data/data/power_system_switch.h"
#include "mc-data/data/push_button.h"
#include "mc-data/data/reflector.h"
#include "mc-data/data/speed_keys.h"
#include "mc-data/data/tps_frequency.h"
#include "mc-data/data/traction_limit.h"
#include "mc-data/data/train_heating.h"
#include "mc-data/data/train_type.h"


#define DUMP_INVALID   "<---UNKNOWN--->"


namespace ModuCab {

/**
 * Dumps newline characters before dump
 * @param stream
 * @param lines
 */
void dumpLines(FILE* stream, unsigned char lines){
	for(unsigned char i = 0; i < lines; i++){
		fprintf(stream, "\n\n\n\n\n\n\n\n\n\n");
	};
} // dumpLines

DataDumper::DataDumper(FILE* stream, unsigned char lines):
	stream(stream),
	lines(lines)
{
	// check
	if(stream == nullptr){
		throw ::std::invalid_argument("Null-pointer in stream given!");
	};
} // constructor

void DataDumper::dump(const ::MCData::Down& data, unsigned char lines){
	dumpLines(this->stream, this->lines + lines);
	
	const char* name;
	
	fprintf(this->stream, "DATA - Down:\n");
	
	/** P-H-L */
	fprintf(this->stream, "\tP-H-L-BA = {value: %5.2f}\n", data.panels.p.h.l.ba.value);
	fprintf(this->stream, "\tP-H-L-BV = {value: %5.2f}\n", data.panels.p.h.l.bv.value);
	
	name = ::MCData::Data::TrainHeating_name(data.panels.p.h.l.u1.value);
	fprintf(this->stream, "\tP-H-L-U1 = {value: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::OnOff_name(data.panels.p.h.l.u2.value);
	fprintf(this->stream, "\tP-H-L-U2 = {value: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PowerSystem_name(data.panels.p.h.l.u3.value);
	fprintf(this->stream, "\tP-H-L-U3 = {value: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::OnOff_name(data.panels.p.h.l.u4.value);
	fprintf(this->stream, "\tP-H-L-U4 = {value: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	fprintf(this->stream, "\tP-H-L-M1 = {value: %6.2f}\n", data.panels.p.h.l.m1.value);
	fprintf(this->stream, "\tP-H-L-M2 = {value: %6.2f}\n", data.panels.p.h.l.m2.value);
	fprintf(this->stream, "\tP-H-L-M3 = {value: %6.2f}\n", data.panels.p.h.l.m3.value);
	fprintf(this->stream, "\tP-H-L-M4 = {value: %6.2f}\n", data.panels.p.h.l.m4.value);
	fprintf(this->stream, "\tP-H-L-M5 = {value: %6.2f}\n", data.panels.p.h.l.m5.value);
	
	/** P-H-P */
	fprintf(this->stream, "\tP-H-P-T1 = {reservoir: %5.2f, pipes: %5.2f}\n", data.panels.p.h.p.t1.pressure_reservoir, data.panels.p.h.p.t1.pressure_pipes);
	fprintf(this->stream, "\tP-H-P-T2 = {cylinders: %5.2f, converter: %5.2f}\n", data.panels.p.h.p.t2.pressure_cylinders, data.panels.p.h.p.t2.pressure_converter);
	
	fprintf(this->stream, "\tP-H-P-P = {value: %5.2f}\n", data.panels.p.h.p.p.value);
	
	fprintf(this->stream, "\tP-H-P-S = {1: %d, 2: %d}\n", data.panels.p.h.p.s1.indication, data.panels.p.h.p.s2.indication);
	
	/** P-D-S */
	fprintf(this->stream, "\tP-D-S-KS = {latch: %d}\n", data.panels.p.d.s.ks.latch);
	
	/** P-D-P */
	fprintf(this->stream, "\tP-D-P-D2 = {%d}\n", data.panels.p.d.p.d2.indication);
	
	/** General */
	fprintf(this->stream, "\tBacklight = {label: %3u, unit: %3u}\n", data.backlight.label, data.backlight.unit);
	fprintf(this->stream, "\tSpeed = {%6.2f}\n", data.speed);
} // dump(const ::MCData::Down&)

void DataDumper::dump(const ::MCData::Up& data, unsigned char lines){
	dumpLines(this->stream, this->lines + lines);
	
	const char* name;
	
	fprintf(this->stream, "DATA - Up:\n");
	
	/** P-H-L */
	name = ::MCData::Data::PushButton_name(data.panels.p.d.l.h1.pressed);
	fprintf(this->stream, "\tP-D-L-H1 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PowerSystemSwitch_name(data.panels.p.d.l.h2.position);
	fprintf(this->stream, "\tP-D-L-H2 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::FallbackControl_name(data.panels.p.d.l.h3.position);
	fprintf(this->stream, "\tP-D-L-H3 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::TrainHeating_name(data.panels.p.d.l.h4.position);
	fprintf(this->stream, "\tP-D-L-H4 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::OnOff_name(data.panels.p.d.l.h5.enabled);
	fprintf(this->stream, "\tP-D-L-H5 = {enabled: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	name = ::MCData::Data::Pantographs_name(data.panels.p.d.l.s1.position);
	fprintf(this->stream, "\tP-D-L-S1 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::Compressor1Aux_name(data.panels.p.d.l.s2.position);
	fprintf(this->stream, "\tP-D-L-S2 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::Compressor2_name(data.panels.p.d.l.s3.position);
	fprintf(this->stream, "\tP-D-L-S3 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::Fans_name(data.panels.p.d.l.s4.position);
	fprintf(this->stream, "\tP-D-L-S4 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	name = ::MCData::Data::Reflector_name(data.panels.p.d.l.d1.position);
	fprintf(this->stream, "\tP-D-L-D1 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::Light_name(data.panels.p.d.l.d2.position);
	fprintf(this->stream, "\tP-D-L-D2 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::Light_name(data.panels.p.d.l.d3.position);
	fprintf(this->stream, "\tP-D-L-D3 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PosLight_name(data.panels.p.d.l.d4.position);
	fprintf(this->stream, "\tP-D-L-D4 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PosLight_name(data.panels.p.d.l.d5.position);
	fprintf(this->stream, "\tP-D-L-D5 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PosLight_name(data.panels.p.d.l.d6.position);
	fprintf(this->stream, "\tP-D-L-D6 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PosLight_name(data.panels.p.d.l.d7.position);
	fprintf(this->stream, "\tP-D-L-D7 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	/** P-D-S */
	name = ::MCData::Data::MCCDirection_name(data.panels.p.d.s.ks.position);
	fprintf(this->stream, "\tP-D-S-KS = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::MCCDriving_name(data.panels.p.d.s.kj.position);
	fprintf(this->stream, "\tP-D-S-KJ = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::MCCBraking_name(data.panels.p.d.s.kb.position);
	fprintf(this->stream, "\tP-D-S-KB = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.s.bl.pressed);
	fprintf(this->stream, "\tP-D-S-BL = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.s.bp.pressed);
	fprintf(this->stream, "\tP-D-S-BP = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	/** P-D-P */
	name = ::MCData::Data::ControlMode_name(data.panels.p.d.p.h1.position);
	fprintf(this->stream, "\tP-D-P-H1 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::DrivingMode_name(data.panels.p.d.p.h2.position);
	fprintf(this->stream, "\tP-D-P-H2 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::TractionLimit_name(data.panels.p.d.p.h3.position);
	fprintf(this->stream, "\tP-D-P-H3 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::TPSFrequency_name(data.panels.p.d.p.h4.position);
	fprintf(this->stream, "\tP-D-P-H4 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	// PDP-K
	
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.p.d1.pressed);
	fprintf(this->stream, "\tP-D-P-D1 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.p.d2.pressed);
	fprintf(this->stream, "\tP-D-P-D2 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::TrainType_name(data.panels.p.d.p.d3.position);
	fprintf(this->stream, "\tP-D-P-D3 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.p.d4.pressed);
	fprintf(this->stream, "\tP-D-P-D4 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	
	/** P-D-B */
	name = ::MCData::Data::Coupling_name(data.panels.p.d.b.p1.position);
	fprintf(this->stream, "\tP-D-B-P1 = {position: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.b.p2.pressed);
	fprintf(this->stream, "\tP-D-B-P2 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
	
	name = ::MCData::Data::PushButton_name(data.panels.p.d.b.p3.pressed);
	fprintf(this->stream, "\tP-D-B-P3 = {pressed: %s}\n", ((name != nullptr) ? name : DUMP_INVALID));
} // dump(const ::MCData::Up&)

} // namespace ModuCab
