/**
***********************************************
***                 ModuCab                 ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab
 * Dumper for printing human-readable content of up/down-stream data
 */
#ifndef MODUCAB_DATADUMPER_H_
#define MODUCAB_DATADUMPER_H_


#include <cstdio> // FILE

#include "mc-data/down.h"
#include "mc-data/up.h"


namespace ModuCab {

class DataDumper {
	public:
		/**
		 * Constructor
		 * @param stream - debugging output
		 * @param lines - number of 10*newline characters to print before the dump itself
		 */
		DataDumper(FILE* stream = stderr, unsigned char lines = 0);
		
		/**
		 * Dumps data
		 * @param data - data-stream to dump
		 */
		void dump(const ::MCData::Down& data, unsigned char lines = 0);
		void dump(const ::MCData::Up& data, unsigned char lines = 0);
	
	
	private:
		/** Debugging output */
		FILE* stream;
		
		/** Newlines (*10) before each dump */
		unsigned char lines;
}; // class DataDumper

} // namespace ModuCab


#endif
