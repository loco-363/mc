/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/combi_meter.h"

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Devices {

CombiMeter::CombiMeter(::ModuCab::Devices::Switches::Debouncer& mux, ::ModuCab::Parts::PCF8591& exp, unsigned char bit_traction, unsigned char bit_pressure):
	mux(mux),
	exp(exp),
	bit_traction(bit_traction),
	bit_pressure(bit_pressure)
{
	// check bits
	if(bit_traction > 7 || bit_pressure > 7){
		throw ::std::invalid_argument("Bit number must be between 0 and 7!");
	};
	
	if(bit_traction == bit_pressure){
		throw ::std::invalid_argument("Bits must be different!");
	};
	
	// set-up expander
	if(!this->exp.dac(true, 0)){
		throw ::std::runtime_error("Unable to set-up DAC!");
	};
} // constructor

CombiMeter::~CombiMeter(){
	// disable DAC output
	this->exp.dac(false, 0);
} // destructor

void CombiMeter::set(unsigned char bit, float value, float zero, float max){
	if((bit > 7 && !(mux.get() & ((1 << this->bit_traction) | (1 << this->bit_pressure)))) || (bit <= 7 && (mux.get() & (1 << bit)))){
		// adjust range
		value -= zero;
		max -= zero;
		
		// check argument
		if(max <= 0){
			return;
		};
		
		// restrict range
		if(value < 0){
			value = 0;
		}
		else if(value > max){
			value = max;
		};
		
		// check and send to expander
		if(value >= 0 && value <= max){// check for NaN
			this->exp.dac(true, value * 255 / max);
		};
	};
} // set

CombiMeter::Device::Device(CombiMeter& meter, unsigned char bit, float zero, float max):
	meter(meter),
	bit(bit),
	zero(zero),
	max(max)
{} // constructor

void CombiMeter::Device::coreData(const byte_t* data, length_t length, length_t offset){
	// as this is very simple device, we do not handle data with offset
	if(data == nullptr || offset || length < sizeof(float)){
		return;
	};
	
	// cast input data
	float value = *((const float*) data);
	
	// send data to CombiMeter
	this->meter.set(this->bit, value, this->zero, this->max);
} // coreData

}} // namespace ModuCab::Devices
