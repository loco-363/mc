/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices
 * Combined meter (simple DAC + voltmeter) - speed, traction & pressure
 */
#ifndef MODUCAB_DEVICES_COMBIMETER_H_
#define MODUCAB_DEVICES_COMBIMETER_H_


#include "core/i_device.h"
#include "devices/switches/debouncer.h"
#include "parts/pcf8591.h"
#include "utils/types.h"


namespace ModuCab { namespace Devices {

class CombiMeter {
	public:
		/**
		 * Constructor
		 * @param mux - reference to I/O input debouncer
		 * @param exp - reference to analog expander
		 * @param bit_up - number of bit in input byte for traction meter
		 * @param bit_down - number of bit in input byte for pressure gauge
		 */
		CombiMeter(::ModuCab::Devices::Switches::Debouncer& mux, ::ModuCab::Parts::PCF8591& exp, unsigned char bit_traction, unsigned char bit_pressure);
		
		/**
		 * Destructor
		 */
		~CombiMeter();
		
		
		/**
		 * Returns device for each meter
		 * @return instance implementing IDevice
		 */
		inline ::ModuCab::Core::IDevice& getDevSpeed(){return this->dev_speed;};
		inline ::ModuCab::Core::IDevice& getDevTraction(){return this->dev_traction;};
		inline ::ModuCab::Core::IDevice& getDevPressure(){return this->dev_pressure;};
	
	
	private:
		/**
		 * Separate device for each meter
		 */
		class Device: public ::ModuCab::Core::IDevice {
			public:
				/**
				 * Constructor
				 * @param meter - reference to CombiMeter instance
				 * @param bit - detect separate meters; for the default one, set to 255
				 * @param zero - value for DAC zero (also the minimum value) (e.g. -5 to make a range -5..max)
				 * @param max - value for DAC maximum
				 */
				Device(CombiMeter& meter, unsigned char bit, float zero, float max);
				
				
				/**
				 * IDevice
				 */
				void coreData(const byte_t* data, length_t length, length_t offset) override;
				inline void coreRequest(length_t length, length_t offset) override {};
			
			
			private:
				CombiMeter& meter;
				unsigned char bit;
				float zero;
				float max;
		}; // class Device
		
		
		/** Reference to I/O input debouncer */
		::ModuCab::Devices::Switches::Debouncer& mux;
		
		/** Reference to analog expander */
		::ModuCab::Parts::PCF8591& exp;
		
		/** Bits of separate meters */
		const unsigned char bit_traction;
		const unsigned char bit_pressure;
		
		/**
		 * Separate devices
		 */
		Device dev_speed = {*this, 255, 0, 100};
		Device dev_traction = {*this, this->bit_traction, -10, 10};
		Device dev_pressure = {*this, this->bit_pressure, 0, 10};
		
		/**
		 * Methods for passing data to output
		 * @param bit - detect separate meters; for the default one, set to 255
		 * @param value - output value
		 * @param zero - value for DAC zero (also the minimum value) (e.g. -5 to make a range -5..max)
		 * @param max - value for DAC maximum
		 */
		void set(unsigned char bit, float value, float zero, float max);
}; // class CombiMeter

}} // namespace ModuCab::Devices


#endif
