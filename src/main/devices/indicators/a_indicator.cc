/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/indicators/a_indicator.h"

#include <unistd.h> // sleep

#include <stdexcept> // C++ standard exceptions


// positions
#define POS_2_TRUE    150
#define POS_2_FALSE   70
#define POS_3_NEGATIVE   195
#define POS_3_ZERO       150
#define POS_3_POSITIVE   105


namespace ModuCab { namespace Devices { namespace Indicators {

AIndicator::AIndicator(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator):
	core(core),
	indicator(indicator)
{
	// set-up indicator
	if(!this->indicator.setPosition(150)){
		throw ::std::runtime_error("Unable to set indicator center position!");
	};
	
	if(!this->indicator.setFrequency(5)){// 50Hz
		throw ::std::runtime_error("Unable to set indicator frequency!");
	};
} // constructor

AIndicator::~AIndicator(){
	// set center position
	this->indicator.setPosition(150);
	
	// wait 1s
	sleep(1);
	
	// stop clock
	this->indicator.setFrequency(0);
} // destructor

bool AIndicator::setPos2(bool pos){
	return this->indicator.setPosition((pos) ? POS_2_TRUE : POS_2_FALSE);
} // setPos2

bool AIndicator::setPos3(char pos){
	if(pos < 0){
		return this->indicator.setPosition(POS_3_NEGATIVE);
	}
	else if(pos > 0){
		return this->indicator.setPosition(POS_3_POSITIVE);
	}
	else {
		return this->indicator.setPosition(POS_3_ZERO);
	};
} // setPos3

}}} // namespace ModuCab::Devices::Indicators
