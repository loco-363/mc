/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Indicators
 * Base abstract indicator
 */
#ifndef MODUCAB_DEVICES_INDICATORS_AINDICATOR_H_
#define MODUCAB_DEVICES_INDICATORS_AINDICATOR_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "parts/indicator.h"


namespace ModuCab { namespace Devices { namespace Indicators {

class AIndicator: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param indicator - reference to Indicator part
		 */
		AIndicator(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator);
		
		/**
		 * Destructor
		 */
		~AIndicator();
	
	
	protected:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to indicator part */
		::ModuCab::Parts::Indicator& indicator;
		
		
		/**
		 * Sets position in 2-position mode
		 * @param pos
		 * @return success/failure
		 */
		bool setPos2(bool pos);
		
		/**
		 * Sets position in 3-position mode
		 * @param pos
		 * @return success/failure
		 */
		bool setPos3(char pos);
}; // class AIndicator

}}} // namespace ModuCab::Devices::Indicators


#endif
