/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/indicators/on_off.h"

#include "mc-data/data/on_off.h"


namespace ModuCab { namespace Devices { namespace Indicators {

OnOff::OnOff(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator):
	AIndicator(core, indicator)
{
	this->setPos2(false);
} // constructor

void OnOff::coreData(const byte_t* data, length_t length, length_t offset){
	// as this is very simple device, we do not handle data with offset
	if(data == nullptr || offset || length < sizeof(::MCData::Data::OnOff)){
		return;
	};
	
	// cast input data
	::MCData::Data::OnOff value = *((const ::MCData::Data::OnOff*) data);
	
	// check and send to indicator
	if(::MCData::Data::OnOff_check(value)){
		this->setPos2(static_cast<bool>(value));
	};
} // coreData

}}} // namespace ModuCab::Devices::Indicators
