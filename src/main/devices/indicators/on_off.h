/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Indicators
 * On/Off indicator
 */
#ifndef MODUCAB_DEVICES_INDICATORS_ONOFF_H_
#define MODUCAB_DEVICES_INDICATORS_ONOFF_H_


#include "devices/indicators/a_indicator.h"


namespace ModuCab { namespace Devices { namespace Indicators {

class OnOff: public AIndicator {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param indicator - reference to Indicator part
		 */
		OnOff(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator);
		
		
		/**
		 * IDevice
		 */
		void coreData(const byte_t* data, length_t length, length_t offset) override;
		inline void coreRequest(length_t length, length_t offset) override {};
}; // class OnOff

}}} // namespace ModuCab::Devices::Indicators


#endif
