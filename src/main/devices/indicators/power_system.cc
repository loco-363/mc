/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/indicators/power_system.h"

#include "mc-data/data/power_system.h"


namespace ModuCab { namespace Devices { namespace Indicators {

PowerSystem::PowerSystem(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator):
	AIndicator(core, indicator)
{
	this->setPos3(0);
} // constructor

void PowerSystem::coreData(const byte_t* data, length_t length, length_t offset){
	// as this is very simple device, we do not handle data with offset
	if(data == nullptr || offset || length < sizeof(::MCData::Data::PowerSystem)){
		return;
	};
	
	// cast input data
	::MCData::Data::PowerSystem value = *((const ::MCData::Data::PowerSystem*) data);
	
	// check and send to indicator
	if(::MCData::Data::PowerSystem_check(value)){
		this->setPos3(static_cast<char>(value));
	};
} // coreData

}}} // namespace ModuCab::Devices::Indicators
