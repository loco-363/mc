/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Indicators
 * Power System indicator
 */
#ifndef MODUCAB_DEVICES_INDICATORS_POWERSYSTEM_H_
#define MODUCAB_DEVICES_INDICATORS_POWERSYSTEM_H_


#include "devices/indicators/a_indicator.h"


namespace ModuCab { namespace Devices { namespace Indicators {

class PowerSystem: public AIndicator {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param indicator - reference to Indicator part
		 */
		PowerSystem(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Indicator& indicator);
		
		
		/**
		 * IDevice
		 */
		void coreData(const byte_t* data, length_t length, length_t offset) override;
		inline void coreRequest(length_t length, length_t offset) override {};
}; // class PowerSystem

}}} // namespace ModuCab::Devices::Indicators


#endif
