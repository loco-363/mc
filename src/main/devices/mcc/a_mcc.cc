/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/mcc/a_mcc.h"

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Devices { namespace MCC {

using ::ModuCab::Parts::Lever;

AMCC::AMCC(::ModuCab::Core::ICoreDownstream& core, Lever& lever):
	core(core),
	lever(lever)
{
	// set-up lever part
	if(!this->lever.setMode(Lever::Mode::Continuous)){
		throw ::std::runtime_error("Unable to set lever mode to Continuous!");
	};
	
	if(!this->lever.setFilter(Lever::Filter::None)){
		throw ::std::runtime_error("Unable to set lever filter to None!");
	};
} // constructor

AMCC::~AMCC(){
	// stop lever data acquisition
	this->lever.setMode(Lever::Mode::Single);
} // destructor

void AMCC::checkPositions(unsigned char pos){
	if(this->lever.getAllPositions() != pos){
		throw ::std::runtime_error("Lever positions mismatch!");
	};
} // checkPositions

}}} // namespace ModuCab::Devices::MCC
