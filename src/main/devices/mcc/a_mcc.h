/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::MCC
 * Main Controller Cylinder - base abstract class to workaround issues with templating in separate h + cc files
 */
#ifndef MODUCAB_DEVICES_MCC_AMCC_H_
#define MODUCAB_DEVICES_MCC_AMCC_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "parts/lever.h"


namespace ModuCab { namespace Devices { namespace MCC {

class AMCC: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param lever - reference to MCC lever part
		 */
		AMCC(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Lever& lever);
		
		/**
		 * Destructor
		 */
		~AMCC();
	
	
	protected:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to lever part */
		::ModuCab::Parts::Lever& lever;
		
		
		/**
		 * Checks number of lever positions and throws an exception if it does not match
		 * @param pos - required number of positions
		 */
		void checkPositions(unsigned char pos);
}; // class AMCC

}}} // namespace ModuCab::Devices::MCC


#endif
