/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/mcc/braking.h"

#include "mc-data/data/mcc_braking.h"


namespace ModuCab { namespace Devices { namespace MCC {

using ::MCData::Data::MCCBraking;
using ::MCData::Data::MCCBraking_check;
using ::MCData::Data::MCCBraking_keysLength;

Braking::Braking(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Lever& lever):
	AMCC(core, lever)
{
	this->checkPositions(MCCBraking_keysLength());
} // constructor

void Braking::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// get actual position and do all the casts
	MCCBraking data = static_cast<MCCBraking>(MCCBraking_keysLength()/2 - this->lever.getPosition());
	
	// check position validity
	if(!MCCBraking_check(data)){
		return;// something bad happened
	};
	
	// send data to core
	this->core.devData(*this, (const byte_t*) &data, sizeof(data));
} // coreRequest

}}} // namespace ModuCab::Devices::MCC
