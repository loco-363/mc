/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::MCC
 * Braking Cylinder
 */
#ifndef MODUCAB_DEVICES_MCC_BRAKING_H_
#define MODUCAB_DEVICES_MCC_BRAKING_H_


#include "devices/mcc/a_mcc.h"


namespace ModuCab { namespace Devices { namespace MCC {

class Braking: public AMCC {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param lever - reference to MCC lever part
		 */
		Braking(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Lever& lever);
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		void coreRequest(length_t length, length_t offset) override;
}; // class Braking

}}} // namespace ModuCab::Devices::MCC


#endif
