/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/mcc/direction.h"

#include "mc-data/data/mcc_direction.h"


namespace ModuCab { namespace Devices { namespace MCC {

using ::MCData::Data::MCCDirection;
using ::MCData::Data::MCCDirection_check;
using ::MCData::Data::MCCDirection_keysLength;

Direction::Direction(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Lever& lever):
	AMCC(core, lever)
{
	this->checkPositions(MCCDirection_keysLength());
} // constructor

void Direction::coreData(const byte_t* data, length_t length, length_t offset){
	//TODO latch
} // coreData

void Direction::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// get actual position and do all the casts
	MCCDirection data = static_cast<MCCDirection>(this->lever.getPosition() - MCCDirection_keysLength()/2);
	
	// check position validity
	if(!MCCDirection_check(data)){
		return;// something bad happened
	};
	
	// send data to core
	this->core.devData(*this, (const byte_t*) &data, sizeof(data));
} // coreRequest

}}} // namespace ModuCab::Devices::MCC
