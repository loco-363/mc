/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/mcc/driving.h"

#include "mc-data/data/mcc_driving.h"


namespace ModuCab { namespace Devices { namespace MCC {

using ::MCData::Data::MCCDriving;
using ::MCData::Data::MCCDriving_check;
using ::MCData::Data::MCCDriving_keysLength;

Driving::Driving(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::Lever& lever):
	AMCC(core, lever)
{
	this->checkPositions(MCCDriving_keysLength());
} // constructor

void Driving::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// get actual position and do all the casts
	MCCDriving data = static_cast<MCCDriving>(this->lever.getPosition() - MCCDriving_keysLength()/2);
	
	// check position validity
	if(!MCCDriving_check(data)){
		return;// something bad happened
	};
	
	// send data to core
	this->core.devData(*this, (const byte_t*) &data, sizeof(data));
} // coreRequest

}}} // namespace ModuCab::Devices::MCC
