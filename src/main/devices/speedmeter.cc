/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/speedmeter.h"

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Devices {

Speedmeter::Speedmeter(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::PCF8591& exp):
	core(core),
	exp(exp)
{
	// set-up expander
	if(!this->exp.dac(true, 0)){
		throw ::std::runtime_error("Unable to set-up DAC!");
	};
} // constructor

Speedmeter::~Speedmeter(){
	// disable DAC output
	this->exp.dac(false, 0);
} // destructor

void Speedmeter::coreData(const byte_t* data, length_t length, length_t offset){
	// as this is very simple device, we do not handle data with offset
	if(data == nullptr || offset || length < sizeof(float)){
		return;
	};
	
	// cast input data
	float value = *((const float*) data);
	
	// restrict range
	if(value < 0){
		value = 0;
	}
	else if(value > 100){
		value = 100;
	};
	
	// check and send to expander
	if(value >= 0 && value <= 100){// check for NaN
		this->exp.dac(true, value * 255 / 100);
	};
} // coreData

}} // namespace ModuCab::Devices
