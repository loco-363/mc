/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices
 * Simple DAC + voltmeter speedmeter
 */
#ifndef MODUCAB_DEVICES_SPEEDMETER_H_
#define MODUCAB_DEVICES_SPEEDMETER_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "parts/pcf8591.h"


namespace ModuCab { namespace Devices {

class Speedmeter: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param exp - reference to expander
		 */
		Speedmeter(::ModuCab::Core::ICoreDownstream& core, ::ModuCab::Parts::PCF8591& exp);
		
		/**
		 * Destructor
		 */
		~Speedmeter();
		
		
		/**
		 * IDevice
		 */
		void coreData(const byte_t* data, length_t length, length_t offset) override;
		inline void coreRequest(length_t length, length_t offset) override {};
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to expander */
		::ModuCab::Parts::PCF8591& exp;
}; // class Speedmeter

}} // namespace ModuCab::Devices


#endif
