/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/switches/debouncer.h"

#include <stdexcept> // C++ standard exceptions


namespace ModuCab { namespace Devices { namespace Switches {

/**
 * Helper for setting/clearing bit
 * @param value
 * @param bit - !!! no checks performed, muset be 0-7 !!!
 * @param set - set if true, clear otherwise
 */
inline void bit(byte_t* value, unsigned char bit, bool set){
	if(set){
		(*value) |= (1 << bit);
	}
	else {
		(*value) &= ~(1 << bit);
	};
} // bit

Debouncer::Debouncer(::ModuCab::Parts::PCF8574& exp, bool inverse, unsigned char count, unsigned char diff):
	exp(exp),
	inverse(inverse),
	count(count),
	diff(diff)
{
	// check arguments
	if(count < 1){
		throw ::std::runtime_error("At least one sample is required!");
	}
	else if(diff > count){
		throw ::std::runtime_error("The difference cannot be bigger than the number of samples!");
	};
	
	// allocate buffer
	this->data = new byte_t[count];
} // constructor

Debouncer::~Debouncer(){
	// free buffer
	delete[] this->data;
} // destructor

bool Debouncer::read(){
	// read new samples
	if(!this->exp.in(this->data, this->count)){
		return false;
	};
	
	// compute states
	unsigned char diff1[8] = {};// should be default-initialized to 0
	
	for(unsigned char i = 0; i < this->count; i++){
		for(unsigned char b = 0; b < 8; b++){
			if(this->data[i] & (1 << b)){
				diff1[b]++;
			};
		};
	};
	
	// debounce & save
	for(unsigned char b = 0; b < 8; b++){
		unsigned char diff0 = this->count - diff1[b];
		
		if(diff1[b] - this->diff >= diff0){// HW=1
			bit(&this->value, b, !this->inverse);
		}
		else if(diff0 - this->diff >= diff1[b]){// HW=0
			bit(&this->value, b, this->inverse);
		};
	};
	
	return true;
} // read

}}} // namespace ModuCab::Devices::Switches
