/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches
 * Reads & debounce input from I/O expander + resolves inverse logic
 */
#ifndef MODUCAB_DEVICES_SWITCHES_DEBOUNCER_H_
#define MODUCAB_DEVICES_SWITCHES_DEBOUNCER_H_


#include "parts/pcf8574.h"
#include "utils/types.h"


namespace ModuCab { namespace Devices { namespace Switches {

class Debouncer {
	public:
		/**
		 * Constructor
		 * @param exp - reference to expander
		 * @param inverse - if true, inverse logic is used in hardware
		 * @param count - number of bytes (samples) to be read
		 * @param diff - number of samples that need to be different to change debounced value
		 */
		Debouncer(::ModuCab::Parts::PCF8574& exp, bool inverse, unsigned char count = 9, unsigned char diff = 2);
		
		/**
		 * Destructor
		 */
		~Debouncer();
		
		
		/**
		 * Returns current debounced value
		 * @return value - logic one means input engaged
		 */
		inline byte_t get(){
			return value;
		}; // get
		
		/**
		 * Reads data from expander and debounce it
		 * @return success/failure
		 */
		bool read();
	
	
	private:
		/** Reference to expander */
		::ModuCab::Parts::PCF8574& exp;
		
		/** True if inversed logic used */
		const bool inverse;
		
		/** Number of samples to read */
		const unsigned char count;
		
		/** Number of different samples required */
		const unsigned char diff;
		
		/** Read data buffer */
		byte_t* data;
		
		/** Current debounced value */
		byte_t value = 0;
}; // class Debouncer

}}} // namespace ModuCab::Devices::Switches


#endif
