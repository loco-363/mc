/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches::Rotary
 * Emulation of rotary switch via two push-buttons
 */
#ifndef MODUCAB_DEVICES_SWITCHES_ROTARY_BUTTONSEQUENCED_CC_
#define MODUCAB_DEVICES_SWITCHES_ROTARY_BUTTONSEQUENCED_CC_


#include <stdexcept> // C++ standard exceptions

#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "devices/switches/debouncer.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Rotary {

/**
 * @param ET - enum type
 * @param BT - enum backing type
 * @param F - pointer to enum check function
 */
template <typename ET, typename BT, bool (*F)(ET)>
class ButtonSequenced: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param input - reference to I/O input debouncer
		 * @param initial - initial switch position
		 * @param bit_up - number of bit in input byte for up button
		 * @param bit_down - number of bit in input byte for down button
		 */
		ButtonSequenced(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, ET initial, unsigned char bit_up, unsigned char bit_down):
			core(core),
			input(input),
			position(initial),
			bit_up(bit_up),
			bit_down(bit_down)
		{
			// check bits
			if(bit_up > 7 || bit_down > 7){
				throw ::std::invalid_argument("Bit number must be between 0 and 7!");
			};
			
			if(bit_up == bit_down){
				throw ::std::invalid_argument("Bits must be different!");
			};
			
			// check initial value
			if(!F(initial)){
				throw ::std::invalid_argument("Given checking function does not accept given initial value!");
			};
		}; // constructor
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		
		void coreRequest(length_t length, length_t offset) override {
			// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
			
			// read new data
			unsigned char in = this->input.get();
			bool in_up = in & (1 << this->bit_up);
			bool in_down = in & (1 << this->bit_down);
			
			// test if both buttons pressed or released, or if waiting for release => do nothing
			if(in_up == in_down || this->pressed){
				// reset flag if both released
				if(!in_up && !in_down){
					this->pressed = false;
				};
				
				return;
			};
			
			// set flag
			this->pressed = true;
			
			// get counting direction
			char diff = (in_up) ? 1 : -1;
			
			// get new position
			BT newpos = static_cast<BT>(this->position) + diff;// do not care about overflow - we do not have any enum now, which will be non-cycle but will define values around overflow edge
			
			// try casting and check validity
			ET np = static_cast<ET>(newpos);
			if(!F(np)){
				return;
			};
			
			// save new position and send it to core
			this->position = np;
			this->core.devData(*this, (const byte_t*) &np, sizeof(np));
		}; // coreRequest
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to I/O input debouncer */
		Debouncer& input;
		
		/** Current switch position */
		ET position;
		
		/** Bits of Up/Down buttons */
		const unsigned char bit_up;
		const unsigned char bit_down;
		
		/** Switch pressed */
		bool pressed = false;
}; // class ButtonSequenced

}}}} // namespace ModuCab::Devices::Switches::Rotary


#endif
