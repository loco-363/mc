/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches::Rotary
 * Emulation of Fallback Control rotary switch via two push-buttons
 */
#ifndef MODUCAB_DEVICES_SWITCHES_ROTARY_FALLBACKCONTROL_H_
#define MODUCAB_DEVICES_SWITCHES_ROTARY_FALLBACKCONTROL_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "devices/switches/debouncer.h"

#include "mc-data/data/fallback_control.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Rotary {

class FallbackControl: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param input - reference to I/O input debouncer
		 * @param bit_up - number of bit in input byte for up button
		 * @param bit_down - number of bit in input byte for down button
		 */
		FallbackControl(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down);
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		void coreRequest(length_t length, length_t offset) override;
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to I/O input debouncer */
		Debouncer& input;
		
		/** Current switch position */
		::MCData::Data::FallbackControl position = ::MCData::Data::FallbackControl::Disabled;
		
		/** Bits of Up/Down buttons */
		const unsigned char bit_up;
		const unsigned char bit_down;
		
		/** Switch pressed */
		bool pressed = false;
}; // class FallbackControl

}}}} // namespace ModuCab::Devices::Switches::Rotary


#endif
