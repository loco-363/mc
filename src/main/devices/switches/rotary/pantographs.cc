/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/switches/rotary/pantographs.h"

#include <stdexcept> // C++ standard exceptions

#include "mc-data/data/pantographs.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Rotary {

/**
 * Returns new switch position from current one and direction of rotation
 * @param current
 * @param right - direction of rotation (true = right)
 * @return next position
 */
::MCData::Data::Pantographs newPos(::MCData::Data::Pantographs current, bool right){
	using PP = ::MCData::Data::Pantographs;
	
	if(right){
		switch(current){
			case PP::None:
				return PP::Rear;
			case PP::Rear:
				return PP::Both;
			case PP::Both:
				return PP::Front;
			case PP::Front:
				return PP::None;
		};
	}
	else {
		switch(current){
			case PP::None:
				return PP::Front;
			case PP::Front:
				return PP::Both;
			case PP::Both:
				return PP::Rear;
			case PP::Rear:
				return PP::None;
		};
	};
	
	return PP::None;// something bad happened
} // newPos

Pantographs::Pantographs(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down):
	core(core),
	input(input),
	bit_up(bit_up),
	bit_down(bit_down)
{
	// check bits
	if(bit_up > 7 || bit_down > 7){
		throw ::std::invalid_argument("Bit number must be between 0 and 7!");
	};
	
	if(bit_up == bit_down){
		throw ::std::invalid_argument("Bits must be different!");
	};
} // constructor

void Pantographs::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// read new data
	unsigned char in = this->input.get();
	bool in_up = in & (1 << this->bit_up);
	bool in_down = in & (1 << this->bit_down);
	
	// test if both buttons pressed or released, or if waiting for release => do nothing
	if(in_up == in_down || this->pressed){
		// reset flag if both released
		if(!in_up && !in_down){
			this->pressed = false;
		};
		
		return;
	};
	
	// set flag
	this->pressed = true;
	
	// get new position - save it and send it to core
	this->position = newPos(this->position, in_up);
	this->core.devData(*this, (const byte_t*) &(this->position), sizeof(this->position));
} // coreRequest

}}}} // namespace ModuCab::Devices::Switches::Rotary
