/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches::Rotary
 * Emulation of Pantographs rotary switch (cycling switch without end-stops) via two push-buttons
 */
#ifndef MODUCAB_DEVICES_SWITCHES_ROTARY_PANTOGRAPHS_H_
#define MODUCAB_DEVICES_SWITCHES_ROTARY_PANTOGRAPHS_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "devices/switches/debouncer.h"

#include "mc-data/data/pantographs.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Rotary {

class Pantographs: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param input - reference to I/O input debouncer
		 * @param bit_up - number of bit in input byte for up button
		 * @param bit_down - number of bit in input byte for down button
		 */
		Pantographs(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down);
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		void coreRequest(length_t length, length_t offset) override;
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to I/O input debouncer */
		Debouncer& input;
		
		/** Current switch position */
		::MCData::Data::Pantographs position = ::MCData::Data::Pantographs::None;
		
		/** Bits of Up/Down buttons */
		const unsigned char bit_up;
		const unsigned char bit_down;
		
		/** Switch pressed */
		bool pressed = false;
}; // class Pantographs

}}}} // namespace ModuCab::Devices::Switches::Rotary


#endif
