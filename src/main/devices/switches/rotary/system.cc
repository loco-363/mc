/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/switches/rotary/system.h"

#include <stdexcept> // C++ standard exceptions

#include "mc-data/data/power_system_switch.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Rotary {

using ::MCData::Data::PowerSystemSwitch;

System::System(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down):
	core(core),
	input(input),
	bit_up(bit_up),
	bit_down(bit_down)
{
	// check bits
	if(bit_up > 7 || bit_down > 7){
		throw ::std::invalid_argument("Bit number must be between 0 and 7!");
	};
	
	if(bit_up == bit_down){
		throw ::std::invalid_argument("Bits must be different!");
	};
} // constructor

void System::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// read new data
	unsigned char in = this->input.get();
	bool in_up = in & (1 << this->bit_up);
	bool in_down = in & (1 << this->bit_down);
	
	// test if both buttons pressed or released, or if waiting for release => do nothing
	if(in_up == in_down || this->pressed){
		// reset flag if both released
		if(!in_up && !in_down){
			this->pressed = false;
			
			// return from start positions
			if(this->position == PowerSystemSwitch::DCStart){
				this->position = PowerSystemSwitch::DCOn;
				this->core.devData(*this, (const byte_t*) &(this->position), sizeof(this->position));
			}
			else if(this->position == PowerSystemSwitch::ACStart){
				this->position = PowerSystemSwitch::ACOn;
				this->core.devData(*this, (const byte_t*) &(this->position), sizeof(this->position));
			};
		};
		
		return;
	};
	
	// set flag
	this->pressed = true;
	
	// get counting direction
	char diff = (in_up) ? 1 : -1;
	
	// get new position
	char newpos = static_cast<char>(this->position) + diff;// do not care about overflow - we do not have any enum now, which will be non-cycle but will define values around overflow edge
	
	// try casting and check validity
	PowerSystemSwitch np = static_cast<PowerSystemSwitch>(newpos);
	if(!::MCData::Data::PowerSystemSwitch_check(np)){
		return;
	};
	
	// save new position and send it to core
	this->position = np;
	this->core.devData(*this, (const byte_t*) &np, sizeof(np));
} // coreRequest

}}}} // namespace ModuCab::Devices::Switches::Rotary
