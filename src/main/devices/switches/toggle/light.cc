/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/switches/toggle/light.h"

#include <stdexcept> // C++ standard exceptions

#include "mc-data/data/light.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Toggle {

Light::Light(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down):
	core(core),
	input(input),
	bit_up(bit_up),
	bit_down(bit_down)
{
	// check bits
	if(bit_up > 7 || bit_down > 7){
		throw ::std::invalid_argument("Bit number must be between 0 and 7!");
	};
	
	if(bit_up == bit_down){
		throw ::std::invalid_argument("Bits must be different!");
	};
} // constructor

void Light::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// read new data
	unsigned char in = this->input.get();
	
	// get new position
	::MCData::Data::Light pos = ::MCData::Data::Light::Off;
	if((in & (1 << this->bit_up))){
		if(!(in & (1 << this->bit_down))){// up position
			pos = ::MCData::Data::Light::Full;
		};
	}
	else if((in & (1 << this->bit_down))){// down position
		pos = ::MCData::Data::Light::Dimmed;
	};
	
	// send new position to core
	this->core.devData(*this, (const byte_t*) &pos, sizeof(pos));
} // coreRequest

}}}} // namespace ModuCab::Devices::Switches::Toggle
