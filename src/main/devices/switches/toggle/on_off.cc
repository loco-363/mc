/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "devices/switches/toggle/on_off.h"

#include <stdexcept> // C++ standard exceptions

#include "mc-data/data/on_off.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Toggle {

OnOff::OnOff(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit):
	core(core),
	input(input),
	bit(bit)
{
	// check bit
	if(bit > 7){
		throw ::std::invalid_argument("Bit number must be between 0 and 7!");
	};
} // constructor

void OnOff::coreRequest(length_t length, length_t offset){
	// ignore parameters - according to IDevice documentation, we have a good reason to do so and respond with the data we want to
	
	// read new data
	unsigned char in = this->input.get();
	
	// get new switch state
	::MCData::Data::OnOff state = (in & (1 << this->bit)) ? ::MCData::Data::OnOff::On : ::MCData::Data::OnOff::Off;
	
	// send new position to core
	this->core.devData(*this, (const byte_t*) &state, sizeof(state));
} // coreRequest

}}}} // namespace ModuCab::Devices::Switches::Toggle
