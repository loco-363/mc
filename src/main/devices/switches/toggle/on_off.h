/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches::Toggle
 * Simple On/Off switch
 */
#ifndef MODUCAB_DEVICES_SWITCHES_TOGGLE_ONOFF_H_
#define MODUCAB_DEVICES_SWITCHES_TOGGLE_ONOFF_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "devices/switches/debouncer.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Toggle {

class OnOff: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param input - reference to I/O input debouncer
		 * @param bit - number of bit in input byte
		 */
		OnOff(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit);
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		void coreRequest(length_t length, length_t offset) override;
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to I/O input debouncer */
		Debouncer& input;
		
		/** Bit of switch */
		const unsigned char bit;
}; // class OnOff

}}}} // namespace ModuCab::Devices::Switches::Toggle


#endif
