/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Devices::Switches::Toggle
 * Position light toggle switch
 */
#ifndef MODUCAB_DEVICES_SWITCHES_TOGGLE_POSLIGHT_H_
#define MODUCAB_DEVICES_SWITCHES_TOGGLE_POSLIGHT_H_


#include "core/i_core_downstream.h"
#include "core/i_device.h"
#include "devices/switches/debouncer.h"


namespace ModuCab { namespace Devices { namespace Switches { namespace Toggle {

class PosLight: public ::ModuCab::Core::IDevice {
	public:
		/**
		 * Constructor
		 * @param core - reference to core
		 * @param input - reference to I/O input debouncer
		 * @param bit_up - number of bit in input byte for up switch position
		 * @param bit_down - number of bit in input byte for down switch position
		 */
		PosLight(::ModuCab::Core::ICoreDownstream& core, Debouncer& input, unsigned char bit_up, unsigned char bit_down);
		
		
		/**
		 * IDevice
		 */
		inline void coreData(const byte_t* data, length_t length, length_t offset) override {};
		void coreRequest(length_t length, length_t offset) override;
	
	
	private:
		/** Reference to core */
		::ModuCab::Core::ICoreDownstream& core;
		
		/** Reference to I/O input debouncer */
		Debouncer& input;
		
		/** Bits of Up/Down switch positions */
		const unsigned char bit_up;
		const unsigned char bit_down;
}; // class PosLight

}}}} // namespace ModuCab::Devices::Switches::Toggle


#endif
