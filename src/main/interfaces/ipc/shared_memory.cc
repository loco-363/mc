/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "interfaces/ipc/shared_memory.h"

#include <cstddef> // size_t
#include <stdexcept> // C++ standard exceptions

#include "mc-data/down.h"
#include "mc-data/up.h"

#include "utils/checking.h"
#include "utils/types.h"


namespace ModuCab { namespace Interfaces { namespace IPC {

using ::std::size_t;
using ::ModuCab::Utils::Checking::checkBufferLength;

/**
 * Returns offset to other data
 * @param offset - current one
 * @return other offset
 */
size_t offsetOther(size_t offset){
	return sizeof(size_t) + ((offset == sizeof(size_t)) ? sizeof(::MCData::Up) : 0);
} // offsetOther

SharedMemory::SharedMemory(const char* name_up, const char* name_down){
	// init up-stream shared memory and check its size
	this->up_shmem = new ::shmem::Client(name_up);
	if(this->up_shmem->getSize() < sizeof(size_t) + 2*sizeof(::MCData::Up)){
		delete this->up_shmem;
		throw ::std::invalid_argument("Insufficient up-stream shared memory size!");
	};
	
	// init down-stream shared memory and weakly check its size (it is the responsibility of the other side - there could be also more copies or even single one)
	try {
		this->down_shmem = new ::shmem::Client(name_down);
		if(this->down_shmem->getSize() < sizeof(size_t) + sizeof(::MCData::Down)){
			delete this->down_shmem;
			throw ::std::invalid_argument("Insufficient down-stream shared memory size!");
		};
	}
	catch(...){// catch any exception
		// free up-stream
		delete this->up_shmem;
		
		// re-throw catched exception
		throw;
	};
	
	// initialize up-stream data
	try {
		// lock up-stream
		this->up_shmem->getMutex().lock();
		
		// init data
		void* addr = this->up_shmem->getAddress();
		this->up_offset = new (addr) size_t;
		new ((void*) ((byte_t*) addr + sizeof(size_t))) ::MCData::Up;
		new ((void*) ((byte_t*) addr + sizeof(size_t) + sizeof(::MCData::Up))) ::MCData::Up;
		
		// set offset to first data
		*(this->up_offset) = sizeof(size_t);
		
		// unlock
		this->up_shmem->getMutex().unlock();
		
		// save address of down-stream offset
		this->down_offset = static_cast<size_t*>(this->down_shmem->getAddress());
	}
	catch(...){// catch any exception
		// no need to separately free the content of shared memories
		
		// free memories
		delete this->up_shmem;
		delete this->down_shmem;
		
		// re-throw catched exception
		throw;
	};
} // constructor

SharedMemory::~SharedMemory(){
	// no need to separately free the content of shared memories
	
	// free memories
	delete this->up_shmem;
	delete this->down_shmem;
} // destructor

int SharedMemory::data(const byte_t* data, length_t length, length_t offset){
	// no need to lock the shared mutex as the other side should have the data read-only and we are modifying the inactive copy
	
	// lock local mutex
	MUTEX_THIS_LOCK(up_mtx)
	
	// check data existence, offset, length
	int ret = checkBufferLength(data, sizeof(::MCData::Up), length, offset);
	if(ret < 0){
		// unlock local mutex
		MUTEX_THIS_UNLOCK(up_mtx)
		
		return ret;
	};
	
	// get offset of inactive data
	size_t off = offsetOther(*(this->up_offset)) + offset;
	
	// get pointer to the beginning of data
	byte_t* p = (byte_t*) this->up_offset + off;
	
	// copy data
	for(length_t i = 0; i < length; i++){
		p[i] = data[i];
	};
	
	// unlock local mutex
	MUTEX_THIS_UNLOCK(up_mtx)
	
	return length;
} // data

int SharedMemory::dataRequest(byte_t* data, length_t length, length_t offset){
	// check data existence, offset, length
	int ret = checkBufferLength(data, sizeof(::MCData::Down), length, offset);
	if(ret < 0){
		return ret;
	};
	
	// lock shared down-stream mutex - will also synchronize local threads
	this->down_shmem->getMutex().lock();
	
	// get pointer to the beginning of data
	const byte_t* p = (const byte_t*) this->down_offset + *(this->down_offset) + offset;
	
	// copy data
	for(length_t i = 0; i < length; i++){
		data[i] = p[i];
	};
	
	// unlock shared down-stream mutex
	this->down_shmem->getMutex().unlock();
	
	return length;
} // dataRequest

bool SharedMemory::commit(bool force){
	// lock local recursive mutex
	MUTEX_THIS_LOCK(up_mtx)
	
	// try to lock up-stream
	if(force){
		this->up_shmem->getMutex().lock();
	}
	else if(!this->up_shmem->getMutex().try_lock()){
		// unlock local mutex
		MUTEX_THIS_UNLOCK(up_mtx)
		
		return false;
	};
	
	// change offset to point to the other data
	*(this->up_offset) = offsetOther(*(this->up_offset));
	
	// unlock it
	this->up_shmem->getMutex().unlock();
	
	// synchronize inactive data to the activated ones
	const byte_t* pa = (const byte_t*) this->up_offset + *(this->up_offset);
	byte_t* pi = (byte_t*) this->up_offset + offsetOther(*(this->up_offset));
	for(length_t i = 0; i < sizeof(::MCData::Up); i++){
		pi[i] = pa[i];
	};
	
	// unlock local mutex
	MUTEX_THIS_UNLOCK(up_mtx)
	
	return true;
} // commit

}}} // namespace ModuCab::Interfaces::IPC
