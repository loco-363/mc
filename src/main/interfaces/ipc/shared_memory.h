/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Interfaces::IPC
 * Shared-Memory Inter-Process-Communication
 */
#ifndef MODUCAB_INTERFACES_IPC_SHAREDMEMORY_H_
#define MODUCAB_INTERFACES_IPC_SHAREDMEMORY_H_


#include <cstddef> // size_t

#include "shmem/src/client.h"

#include "interfaces/i_generic.h"
#include "utils/threading/mutex.h"


namespace ModuCab { namespace Interfaces { namespace IPC {

class SharedMemory: public ::ModuCab::Interfaces::IGeneric {
	public:
		/**
		 * Constructor
		 * @param name_up - name up-stream data shared memory ID
		 * @param name_down - name down-stream data shared memory ID
		 */
		SharedMemory(const char* name_up, const char* name_down);
		
		/**
		 * Destructor
		 */
		~SharedMemory();
		
		
		/**
		 * IGeneric
		 */
		int data(const byte_t* data, length_t length, length_t offset) override;
		int dataRequest(byte_t* data, length_t length, length_t offset) override;
		inline int dataRequest(length_t length, length_t offset) override {return 0;};// unsupported as it will need additional copy of data or will cause the other process to wait
		
		
		/**
		 * Commits changes to up-stream data
		 * @param force - if true, block until commited
		 * @return commited or not
		 */
		bool commit(bool force = true);
	
	
	private:
		/** Pointer to up-stream shared memory */
		::shmem::Client* up_shmem;
		
		/** Offset of up-stream data */
		::std::size_t* up_offset;
		
		/** Mutex for local-locking of up-stream data when changing - needs to be recursive to cover also commit from data */
		MUTEX_RECURSIVE(up_mtx)
		
		/** Pointer to down-stream shared memory */
		::shmem::Client* down_shmem;
		
		/** Offset of down-stream data */
		::std::size_t* down_offset;
}; // class SharedMemory

}}} // namespace ModuCab::Interfaces::IPC


#endif
