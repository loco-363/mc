/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <signal.h> // POSIX signals
#include <unistd.h> // usleep

#include <cstdio> // fprintf
#include <stddef.h> // offsetof

#include "core/sequential.h"
#include "devices/combi_meter.h"
#include "devices/indicators/on_off.h"
#include "devices/indicators/power_system.h"
#include "devices/mcc/braking.h"
#include "devices/mcc/direction.h"
#include "devices/mcc/driving.h"
#include "devices/switches/debouncer.h"
#include "devices/switches/rotary/fallback_control.h"
#include "devices/switches/rotary/pantographs.h"
#include "devices/switches/rotary/reflector.h"
#include "devices/switches/rotary/system.h"
#include "devices/switches/toggle/light.h"
#include "devices/switches/toggle/on_off.h"
#include "devices/switches/toggle/pos_light.h"
#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/ipc/shared_memory.h"
#include "interfaces/serial/posix.h"
#include "parts/indicator.h"
#include "parts/lever.h"
#include "parts/pcf8574.h"
#include "parts/pcf8591.h"
#include "utils/types.h"
#include "utils/types_core.h"

#include "mc-data/down.h"
#include "mc-data/up.h"


// main loop interval
#define INTERVAL 20000

// macros for sleeping
#if defined(INTERVAL) && INTERVAL > 0
	#define INTERVAL_SLEEP   usleep(INTERVAL);
#else
	#define INTERVAL_SLEEP
#endif


/** Variable to allow graceful termination of main loop */
bool run = true;


/**
 * Handles SIGINT and SIGTERM signals => switch variable to gracefully terminate process at the end of main loop
 * @param int sig - the signal to handle
 */
void signalHandler(int sig){
	if(sig == SIGINT || sig == SIGTERM){
		run = false;
	};
} // signalHandler

/**
 * MAIN
 */
int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	}
	else if(argc < 3){
		fprintf(stderr, "Missing Shared Memory ID for up-data.\n");
		return -1;
	}
	else if(argc < 4){
		fprintf(stderr, "Missing Shared Memory ID for down-data.\n");
		return -1;
	};
	
	run = true;
	
	// register signal handler to allow graceful termination
	struct sigaction act;
	act.sa_handler = signalHandler;
	if(sigaction(SIGINT, &act, 0) < 0 || sigaction(SIGTERM, &act, 0) < 0){
		fprintf(stderr, "ERROR - Binding to signals failed!\n");
		return -2;
	};
	
	using namespace ModuCab;
	
	// init up-stream interface
	Interfaces::IPC::SharedMemory iface = {argv[2], argv[3]};
	
	// init interface
	Interfaces::Serial::Posix serial = {
		argv[1],
		Interfaces::Serial::Posix::Baudrate::BR115200,
		3
	};
	fprintf(stderr, "Serial UP\n");
	
	// init BusPirate into I2C mode
	Interfaces::I2C::Drivers::BusPirateV4 bp = {
		serial,
		Interfaces::I2C::Drivers::BusPirateV4::ModeI2C::Hardware_100kHz,
		Interfaces::I2C::Drivers::BusPirateV4::PullUps::External
	};
	fprintf(stderr, "BP constructed\n");
	
	// init I2C bus
	static Interfaces::I2C::I2C bus = {bp};
	fprintf(stderr, "Bus created\n\n");
	
	// init parts
	static struct {
		struct {
			Parts::Lever direction = {bus, 0x21};
			Parts::Lever driving = {bus, 0x22};
			Parts::Lever braking = {bus, 0x23};
		} mcc;
		
		struct {
			Parts::Indicator u2 = {bus, 0x32};
			Parts::Indicator u3 = {bus, 0x33};
			Parts::Indicator u4 = {bus, 0x34};
		} indicators;
		
		struct {
			Parts::PCF8591 analog = {bus, 0x48};
			Parts::PCF8574 io = {bus, 0x38, Parts::PCF8574::Mode::Input};
			
			struct {
				Parts::PCF8574 buttons = {bus, 0x25, Parts::PCF8574::Mode::Input};
				Parts::PCF8574 switches_upper = {bus, 0x26, Parts::PCF8574::Mode::Input};
				Parts::PCF8574 switches_lower = {bus, 0x27, Parts::PCF8574::Mode::Input};
			} box;
		} expanders;
	} parts;
	
	// init I/O expanders debouncers
	static struct {
		Devices::Switches::Debouncer io = {parts.expanders.io, true};
		
		struct {
			Devices::Switches::Debouncer buttons = {parts.expanders.box.buttons, true};
			Devices::Switches::Debouncer switches_upper = {parts.expanders.box.switches_upper, true};
			Devices::Switches::Debouncer switches_lower = {parts.expanders.box.switches_lower, true};
		} box;
	} debouncers;
	
	// set-up parts
	if(!parts.expanders.analog.adc(Parts::PCF8591::Inputs::Single, Parts::PCF8591::Channel::All)){
		fprintf(stderr, "Error during parts set-up!\n");
		return -1;
	};
	
	// init data
	MCData::Down data_down;
	MCData::Up data_up;
	
	// init core
	static Core::Sequential core = {
		iface,
		(byte_t*) &data_down, sizeof(data_down),
		(byte_t*) &data_up, sizeof(data_up)
	};
	
	// init devices
	struct {
		struct {
			Devices::MCC::Direction direction = {core, parts.mcc.direction};
			Devices::MCC::Driving driving = {core, parts.mcc.driving};
			Devices::MCC::Braking braking = {core, parts.mcc.braking};
		} mcc;
		
		struct {
			Devices::Indicators::OnOff u2 = {core, parts.indicators.u2};
			Devices::Indicators::PowerSystem u3 = {core, parts.indicators.u3};
			Devices::Indicators::OnOff u4 = {core, parts.indicators.u4};
		} indicators;
		
		struct {
			Devices::Switches::Rotary::FallbackControl fallback = {core, debouncers.box.buttons, 6, 7};
			Devices::Switches::Rotary::Reflector reflector = {core, debouncers.box.buttons, 4, 5};
			Devices::Switches::Rotary::System system = {core, debouncers.box.buttons, 2, 3};
			Devices::Switches::Rotary::Pantographs pantographs = {core, debouncers.box.buttons, 0, 1};
			
			Devices::Switches::Toggle::OnOff main = {core, debouncers.box.switches_upper, 5};
			
			Devices::Switches::Toggle::Light light_panel = {core, debouncers.box.switches_upper, 3, 2};
			Devices::Switches::Toggle::Light light_cabin = {core, debouncers.box.switches_upper, 1, 0};
			
			struct {
				struct {
					Devices::Switches::Toggle::PosLight left = {core, debouncers.box.switches_lower, 7, 6};
					Devices::Switches::Toggle::PosLight right = {core, debouncers.box.switches_lower, 5, 4};
				} rear;
				
				struct {
					Devices::Switches::Toggle::PosLight left = {core, debouncers.box.switches_lower, 3, 2};
					Devices::Switches::Toggle::PosLight right = {core, debouncers.box.switches_lower, 1, 0};
				} front;
			} poslights;
		} box;
		
		Devices::CombiMeter combimeter = {debouncers.box.switches_upper, parts.expanders.analog, 7, 6};
	} devices;
	
	// init core devices array
	const devlength_t devs_length = 20;//TODO
	const device_t devs[devs_length] = {// specifying length prevents passing different length to core
		{
			devices.mcc.direction,
			{offsetof(MCData::Down, panels.p.d.s.ks), sizeof(data_down.panels.p.d.s.ks)},// down
			{offsetof(MCData::Up, panels.p.d.s.ks), sizeof(data_up.panels.p.d.s.ks)}// up
		},
		{
			devices.mcc.driving,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.s.kj), sizeof(data_up.panels.p.d.s.kj)}// up
		},
		{
			devices.mcc.braking,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.s.kb), sizeof(data_up.panels.p.d.s.kb)}// up
		},
		{
			devices.indicators.u2,
			{offsetof(MCData::Down, panels.p.h.l.u2), sizeof(data_down.panels.p.h.l.u2)},// down
			{0, 0}// up
		},
		{
			devices.indicators.u3,
			{offsetof(MCData::Down, panels.p.h.l.u3), sizeof(data_down.panels.p.h.l.u3)},// down
			{0, 0}// up
		},
		{
			devices.indicators.u4,
			{offsetof(MCData::Down, panels.p.h.l.u4), sizeof(data_down.panels.p.h.l.u4)},// down
			{0, 0}// up
		},
		{
			devices.combimeter.getDevSpeed(),
			{offsetof(MCData::Down, speed), sizeof(data_down.speed)},// down
			{0, 0}// up
		},
		{
			devices.combimeter.getDevTraction(),
			{offsetof(MCData::Down, panels.p.h.p.p), sizeof(data_down.panels.p.h.p.p)},// down
			{0, 0}// up
		},
		{
			devices.combimeter.getDevPressure(),
			{offsetof(MCData::Down, panels.p.h.p.t2.pressure_converter), sizeof(data_down.panels.p.h.p.t2.pressure_converter)},// down
			{0, 0}// up
		},
		{
			devices.box.fallback,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.h3), sizeof(data_up.panels.p.d.l.h3)}// up
		},
		{
			devices.box.reflector,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d1), sizeof(data_up.panels.p.d.l.d1)}// up
		},
		{
			devices.box.system,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.h2), sizeof(data_up.panels.p.d.l.h2)}// up
		},
		{
			devices.box.pantographs,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.s1), sizeof(data_up.panels.p.d.l.s1)}// up
		},
		{
			devices.box.main,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.h5), sizeof(data_up.panels.p.d.l.h5)}// up
		},
		{
			devices.box.light_panel,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d3), sizeof(data_up.panels.p.d.l.d3)}// up
		},
		{
			devices.box.light_cabin,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d2), sizeof(data_up.panels.p.d.l.d2)}// up
		},
		{
			devices.box.poslights.rear.left,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d7), sizeof(data_up.panels.p.d.l.d7)}// up
		},
		{
			devices.box.poslights.rear.right,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d6), sizeof(data_up.panels.p.d.l.d6)}// up
		},
		{
			devices.box.poslights.front.left,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d5), sizeof(data_up.panels.p.d.l.d5)}// up
		},
		{
			devices.box.poslights.front.right,
			{0, 0},// down
			{offsetof(MCData::Up, panels.p.d.l.d4), sizeof(data_up.panels.p.d.l.d4)}// up
		}
	};
	
	// inject devices to core
	core.injectDevices(devs, devs_length);
	
	// main loop
	while(run){
		// read new data for debouncers
		debouncers.box.buttons.read();
		debouncers.box.switches_upper.read();
		debouncers.box.switches_lower.read();
		
		// do a core cycle
		core.cycle();
		
		// force commit up-stream data
		iface.commit();
		
		// sleep to next step
		INTERVAL_SLEEP
	};
	
	return 0;
} // main
