/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "parts/indicator.h"


// Communication
#define COMM_POS_GET   0x20
#define COMM_POS_SET   0x21
#define COMM_FREQ_GET   0x22
#define COMM_FREQ_SET   0x23


namespace ModuCab { namespace Parts {

Indicator::Indicator(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address):
	bus(bus),
	address(address & 0x7F)
{} // constructor

byte_t Indicator::getPosition(){
	byte_t pos = 0;
	this->bus.bytes_8(this->address, COMM_POS_GET, &pos, 1);
	return pos;
} // getPosition

bool Indicator::setPosition(byte_t pos){
	return (this->bus.write_16(this->address, COMM_POS_SET, pos) == 2);
} // setPosition

byte_t Indicator::getFrequency(){
	byte_t freq = 0;
	this->bus.bytes_8(this->address, COMM_FREQ_GET, &freq, 1);
	return freq;
} // getFrequency

bool Indicator::setFrequency(byte_t freq){
	return (this->bus.write_16(this->address, COMM_FREQ_SET, freq) == 2);
} // setFrequency

}} // namespace ModuCab::Parts
