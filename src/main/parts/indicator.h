/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Parts
 * Simple servo indicator
 */
#ifndef MODUCAB_PARTS_INDICATOR_H_
#define MODUCAB_PARTS_INDICATOR_H_


#include "interfaces/i2c/i_bus.h"
#include "utils/types.h"


namespace ModuCab { namespace Parts {

class Indicator {
	public:
		/**
		 * Constructor
		 * @param bus - reference to I2C bus
		 * @param address - indicator address (7-bit)
		 */
		Indicator(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address);
		
		
		/**
		 * Returns servo position
		 * @return servo position as pulse duration in 10us (e.g. 150 = 1.5ms pulse)
		 */
		byte_t getPosition();
		
		/**
		 * Sets servo position
		 * @param pos - servo position as pulse duration in 10us (e.g. for 1.5ms pulse send 150 decimal)
		 */
		bool setPosition(byte_t pos);
		
		/**
		 * Returns servo PWM frequency
		 * @return frequency in 10Hz (e.g. 5 = 50Hz)
		 */
		byte_t getFrequency();
		
		/**
		 * Sets servo PWM frequency
		 * @param freq - frequency in 10Hz (e.g. for 50Hz send 5)
		 */
		bool setFrequency(byte_t freq);
	
	
	private:
		/** Reference to I2C bus */
		::ModuCab::Interfaces::I2C::IBus& bus;
		
		/** Lever 7-bit address */
		byte_t address;
}; // class Indicator

}} // namespace ModuCab::Parts


#endif
