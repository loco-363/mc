/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include "parts/lever.h"


// Communication
#define COMM_VALUE   0x20
#define COMM_POSITIONS   0x21
#define COMM_START   0x30
#define COMM_MODE_GET   0x40
#define COMM_MODE_SET   0x41
#define COMM_FILTER_GET   0x50
#define COMM_FILTER_SET   0x51


// separate write & read calls necessary as MCU does not handle data before STOP condition
// (repeated START condition is not a problem, but the data are not prepared for immediate reading)

namespace ModuCab { namespace Parts {

Lever::Lever(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address):
	bus(bus),
	address(address & 0x7F)
{} // constructor

byte_t Lever::getPosition(){
	this->bus.write_8(this->address, COMM_VALUE);
	byte_t pos = (1 << 7);
	this->bus.read(this->address, &pos, 1);
	return pos;
} // getPosition

byte_t Lever::getAllPositions(){
	this->bus.write_8(this->address, COMM_POSITIONS);
	byte_t pos = 0;
	this->bus.read(this->address, &pos, 1);
	return pos;
} // getAllPositions

Lever::Mode Lever::getMode(){
	this->bus.write_8(this->address, COMM_MODE_GET);
	byte_t mode = 0;
	this->bus.read(this->address, &mode, 1);
	return (mode < 3) ? static_cast<Lever::Mode>(mode) : Lever::Mode::Single;
} // getMode

bool Lever::setMode(Lever::Mode mode){
	return (this->bus.write_8(this->address, (COMM_MODE_SET + static_cast<byte_t>(mode))) == 1);
} // setMode

bool Lever::start(){
	return (this->bus.write_8(this->address, COMM_START) == 1);
} // start

Lever::Filter Lever::getFilter(){
	this->bus.write_8(this->address, COMM_FILTER_GET);
	byte_t mode = 0;
	this->bus.read(this->address, &mode, 1);
	return (mode < 6) ? static_cast<Lever::Filter>(mode) : Lever::Filter::None;
} // getFilter

bool Lever::setFilter(Lever::Filter mode){
	return (this->bus.write_8(this->address, (COMM_FILTER_SET + static_cast<byte_t>(mode))) == 1);
} // setFilter

}} // namespace ModuCab::Parts
