/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * ModuCab::Parts
 * Main Controller Cylinder Lever
 */
#ifndef MODUCAB_PARTS_LEVER_H_
#define MODUCAB_PARTS_LEVER_H_


#include "interfaces/i2c/i_bus.h"
#include "utils/types.h"


namespace ModuCab { namespace Parts {

class Lever {
	public:
		/**
		 * Modes of data acquisition
		 */
		enum class Mode : byte_t {
			Single = 0,
			SingleActive,
			Continuous
		}; // enum Mode
		
		/**
		 * Modes of input filtering
		 */
		enum class Filter : byte_t {
			None = 0,
			Background,
			SequencedSimple,
			SequencedSimpleBackground,
			Sequenced,
			SequencedBackground
		}; // enum Mode
		
		
		/**
		 * Constructor
		 * @param bus - reference to I2C bus
		 * @param address - lever address (7-bit)
		 */
		Lever(::ModuCab::Interfaces::I2C::IBus& bus, byte_t address);
		
		
		/**
		 * Reads actual lever position
		 * @return position - most significant bit signalizes validity (if set, the value is old or the lever is somewhere between valid positions)
		 */
		byte_t getPosition();
		
		/**
		 * Get number of positions
		 * @return number of positions
		 */
		byte_t getAllPositions();
		
		/**
		 * Get data acquisition mode
		 * @return mode
		 */
		Mode getMode();
		
		/**
		 * Set data acquisition mode
		 * @param mode
		 * @return success/failure
		 */
		bool setMode(Mode mode);
		
		/**
		 * Starts acquisition in single-mode
		 * @return success/failure
		 */
		bool start();
		
		/**
		 * Get input filtering mode
		 * @return mode
		 */
		Filter getFilter();
		
		/**
		 * Set input filtering mode
		 * @param mode
		 * @return success/failure
		 */
		bool setFilter(Filter mode);
	
	
	private:
		/** Reference to I2C bus */
		::ModuCab::Interfaces::I2C::IBus& bus;
		
		/** Lever 7-bit address */
		byte_t address;
}; // class Lever

}} // namespace ModuCab::Parts


#endif
