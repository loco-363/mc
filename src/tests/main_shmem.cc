/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <signal.h> // POSIX signals
#include <unistd.h> // close, execl, fork, sleep, usleep
#include <sys/types.h> // pid_t
#include <sys/wait.h> // wait

#include <cstddef> // size_t
#include <cstdio> // fprintf

#include "shmem/src/manager.h"

#include "mc-data/down.h"
#include "mc-data/up.h"

#include "data_dumper.h"
#include "utils/types.h"


// main loop interval
#define INTERVAL 100000

// shared memory IDs
#define SHMEM_ID_UP   "Loco-363_Modu-Cab_test_UP"
#define SHMEM_ID_DOWN   "Loco-363_Modu-Cab_test_DOWN"


/** Variable to allow graceful termination of main loop */
bool run = true;


/**
 * Handles SIGINT and SIGTERM signals => switch variable to gracefully terminate process at the end of main loop
 * @param int sig - the signal to handle
 */
void signalHandler(int sig){
	if(sig == SIGINT || sig == SIGTERM){
		run = false;
	};
} // signalHandler

/**
 * MAIN
 */
int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	run = true;
	
	// register signal handler to allow graceful termination
	struct sigaction act;
	act.sa_handler = signalHandler;
	if(sigaction(SIGINT, &act, 0) < 0 || sigaction(SIGTERM, &act, 0) < 0){
		fprintf(stderr, "ERROR - Binding to signals failed!\n");
		return -2;
	};
	
	// init shared-memory
	using ModuCab::byte_t;
	using std::size_t;
	
	shmem::Manager shmem_up = {
		SHMEM_ID_UP,
		sizeof(size_t) + 2*sizeof(MCData::Up)
	};
	shmem::Manager shmem_down = {
		SHMEM_ID_DOWN,
		sizeof(size_t) + sizeof(MCData::Down)
	};
	
	// init shared-memory down-stream data
	void* addr = shmem_down.getAddress();
	size_t* shmem_down_offset = new (addr) size_t;
	MCData::Down* shmem_down_data = new ((void*) ((byte_t*) addr + sizeof(size_t))) MCData::Down;
	*shmem_down_offset = sizeof(size_t);
	
	// get shared-memory up-stream offset
	size_t* shmem_up_offset = static_cast<size_t*>(shmem_up.getAddress());
	
	// init up-stream data copy
	MCData::Up data_up;
	
	// check if we can continue
	if(!run){
		return 0;
	};
	
	// fork main process
	pid_t pid = fork();
	if(pid < 0){// error
		fprintf(stderr, "ERROR - Fork failed!\n");
		return 2;
	}
	else if(pid == 0){// child
		// close stdin & stdout
		if(close(0) < 0 || close(1) < 0){
			fprintf(stderr, "ERROR (child) - Closing of standard in or out failed!\n");
			return 3;
		};
		
		// run main process
		execl("bin/main", argv[0], argv[1], SHMEM_ID_UP, SHMEM_ID_DOWN, (char *) NULL);
		
		// error when replacing process image (otherwise execl never returns)
		perror("ERROR (child) - Process image replacing failed");
		return 4;
	};
	
	// wait for start of main process - otherwise the up-stream offset and data will be invalid
	sleep(5);
	
	// init data dumper
	ModuCab::DataDumper dumper = {stdout};
	
	// main loop
	while(run){
		// dump down-stream data - no need for locking as we emulate the source side
		dumper.dump(*shmem_down_data, 1);
		
		// copy up-stream data
		shmem_up.getMutex().lock();
		
		byte_t* p = (byte_t*) &data_up;
		byte_t* q = (byte_t*) shmem_up_offset + *shmem_up_offset;
		for(ModuCab::length_t i = 0; i < sizeof(MCData::Up); i++){
			p[i] = q[i];
		};
		
		shmem_up.getMutex().unlock();
		
		// dump up-stream data
		dumper.dump(data_up);
		
		// sleep to next dump
		usleep(INTERVAL);
	};
	
	// signal main process & wait for its termination
	kill(pid, SIGTERM);
	wait(NULL);
	
	return 0;
} // main
