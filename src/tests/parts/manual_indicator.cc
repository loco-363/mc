/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <unistd.h> // sleep, usleep

#include <cstdio> // fprintf, printf

#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/posix.h"
#include "parts/indicator.h"
#include "utils/types.h"


#define INTERVAL 50000 // 50 ms


using ModuCab::byte_t;
using ModuCab::Parts::Indicator;
using ModuCab::Interfaces::I2C::I2C;
using ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// init BusPirate into I2C mode
	BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	printf("\nBP constructed\n\n");
	
	// init I2C bus
	I2C bus = {bp, true};
	
	// init part
	Indicator indicator = {bus, 0x30};
	
	// get frequency
	printf("FREQ = %u Hz\n", indicator.getFrequency() * 10);
	
	// get actual position
	printf("POS = %u\n", indicator.getPosition());
	
	// slow move
	for(byte_t i = 60; i < 240; i++){
		indicator.setPosition(i);
		
		usleep(INTERVAL);
	};
	for(byte_t i = 240; i > 60; i--){
		indicator.setPosition(i);
		
		usleep(INTERVAL);
	};
	
	sleep(2);
	
	// fast move
	indicator.setPosition(240);
	sleep(2);
	indicator.setPosition(150);
	
	return 0;
} // main
