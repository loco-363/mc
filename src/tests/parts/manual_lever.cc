/**
***********************************************
***            Loco363 - ModuCab            ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <unistd.h> // usleep

#include <cstdio> // fprintf, printf

#include "tests/testing_tools/serial_debugger.h"

#include "interfaces/i2c/i2c.h"
#include "interfaces/i2c/drivers/bus_pirate_v4.h"
#include "interfaces/serial/posix.h"
#include "parts/lever.h"


#define INTERVAL 100000 // 100 ms


using ModuCab::Parts::Lever;
using ModuCab::Interfaces::I2C::I2C;
using ModuCab::Interfaces::I2C::Drivers::BusPirateV4;
using ModuCab::Interfaces::Serial::Posix;


int main(int argc, char **argv){
	// check arguments
	if(argc < 2){
		fprintf(stderr, "Missing file-name argument.\n");
		return -1;
	};
	
	// init interface
	Posix serial = {argv[1], Posix::Baudrate::BR115200, 3};
	printf("Serial UP\n");
	
	// init debugger
	SerialDebugger debugger = {serial};
	printf("Debugger hooked up\n");
	
	// init BusPirate into I2C mode
	BusPirateV4 bp = {debugger, BusPirateV4::ModeI2C::Hardware_100kHz, BusPirateV4::PullUps::Internal5V};
	printf("\nBP constructed\n\n");
	
	// init I2C bus
	I2C bus = {bp, true};
	
	// init part
	Lever lever = {bus, 0x20};
	
	// start continuous measurement
	if(!lever.setMode(Lever::Mode::Continuous)){
		return -1;
	};
	
	// set None filter mode
	if(!lever.setFilter(Lever::Filter::None)){
		return -1;
	};
	
	// get number of positions
	printf("AllPOS=%u\n", lever.getAllPositions());
	
	// read actual position
	for(unsigned char i = 0; i < 100; i++){
		printf("pos=%u\n", lever.getPosition());
		
		usleep(INTERVAL);
	};
	
	// set to single & stop mode
	if(!lever.setMode(Lever::Mode::Single)){
		return -1;
	};
	
	return 0;
} // main
